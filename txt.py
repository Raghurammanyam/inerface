import cv2
import numpy as np
import pytesseract
import imutils
from cv2 import boundingRect, countNonZero, cvtColor, drawContours, findContours, getStructuringElement, imread, morphologyEx, pyrDown, rectangle, threshold
#42115passportImage1565181949243.jpg
#42450passportImage1565181903846.jpg
large = imread('/home/raghu/psimage/92298passportImage1565181939609.jpg')
# downsample and use it for processing
rgb = pyrDown(large)
# apply grayscale
rgb = imutils.resize(rgb, height=600)
small = cvtColor(rgb, cv2.COLOR_BGR2GRAY)
# morphological gradient
morph_kernel = getStructuringElement(cv2.MORPH_ELLIPSE, (22, 4))
grad = morphologyEx(small, cv2.MORPH_GRADIENT, morph_kernel)
# binarize
_, bw = threshold(src=grad, thresh=0, maxval=255, type=cv2.THRESH_BINARY+cv2.THRESH_OTSU)
morph_kernel = getStructuringElement(cv2.MORPH_RECT, (22, 9))
# connect horizontally oriented regions
connected = morphologyEx(bw, cv2.MORPH_CLOSE, morph_kernel)
mask = np.zeros(bw.shape, np.uint8)
# find contours
contours, hierarchy = findContours(connected, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
# filter contours
for idx in range(0, len(hierarchy[0])):
    rect = x, y, rect_width, rect_height = boundingRect(contours[idx])
    # fill the contour
    mask = drawContours(mask, contours, idx, (255, 255, 2555), cv2.FILLED)
    # ratio of non-zero pixels in the filled region
    r = float(countNonZero(mask)) / (rect_width * rect_height)
    if r > 5 and rect_height > 8 and rect_width > 8:
        roi=rgb[y: y+rect_height, x:x+rect_width]
        rgb = rectangle(rgb, (x, y+rect_height), (x+rect_width, y), (255,255,255),3)
        config = ("-l eng --oem 1 --psm 9")
        text = pytesseract.image_to_string(roi, config=config)
        print(text)
        cv2.imshow("idx",rgb)
        # cv2.waitKey(0)
        
cv2.imwrite("final.jpeg",rgb)
# cv2.waitKey(0)
#(254, 33) (319, 21)
#(241, 47) (309, 36)
