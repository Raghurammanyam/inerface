# USAGE
# python detect_mrz.py --images examples

# import the necessary packages
from imutils import paths
import numpy as np
import argparse
import imutils
import cv2
import os
from datetime import datetime
cwd =os.getcwd()
# construct the argument parse and parse the arguments


# initialize a rectangular and square structuring kernel
rectKernel = cv2.getStructuringElement(cv2.MORPH_RECT, (55, 10))
sqKernel = cv2.getStructuringElement(cv2.MORPH_RECT, (44, 44))
morph_path=cwd+'/'+str(datetime.now())+'.jpeg'
# loop over the input image paths
# for imagePath in paths.list_images(args["images"]):
# load the image, resize it, and convert it to grayscale
image = cv2.pyrDown(cv2.imread("/home/raghu/mrz/image1.jpeg"))
image = imutils.resize(image, height=600)
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
cv2.imshow("image",image)
cv2.waitKey(0)
# cv2.imshow("ROI", gray)
# cv2.waitKey(0)
# smooth the image using a 3x3 Gaussian, then apply the blackhat
# morphological operator to find dark regions on a light background
gray = cv2.GaussianBlur(gray, (3, 3), 0)
blackhat = cv2.morphologyEx(gray, cv2.MORPH_BLACKHAT, rectKernel)
# cv2.imshow("ROI", blackhat)
# cv2.waitKey(0)
# compute the Scharr gradient of the blackhat image and scale the
# result into the range [0, 255]
gradX = cv2.Sobel(blackhat, ddepth=cv2.CV_32F, dx=1, dy=0, ksize=-1)
gradX = np.absolute(gradX)
(minVal, maxVal) = (np.min(gradX), np.max(gradX))
gradX = (255 * ((gradX - minVal) / (maxVal - minVal))).astype("uint8")

# apply a closing operation using the rectangular kernel to close
# gaps in between letters -- then apply Otsu's thresholding method
gradX = cv2.morphologyEx(gradX, cv2.MORPH_CLOSE, rectKernel)
thresh = cv2.threshold(gradX, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
# cv2.imshow("thresh", thresh)
# cv2.waitKey(0)
# perform another closing operation, this time using the square
# kernel to close gaps between lines of the MRZ, then perform a
# serieso of erosions to break apart connected components
thresh = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, sqKernel)
thresh = cv2.erode(thresh, None, iterations=4)
# cv2.imshow("erode", thresh)
# cv2.waitKey(0)
# during thresholding, it's possible that border pixels were
# included in the thresholding, so let's set 5% of the left and
# right borders to zero
p = int(image.shape[1] * 0.05)
thresh[:, 0:p] = 0
thresh[:, image.shape[1] - p:] = 0

# find contours in the thresholded image and sort them by their
# size
cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
	cv2.CHAIN_APPROX_SIMPLE)
cnts = imutils.grab_contours(cnts)
cnts = sorted(cnts, key=cv2.contourArea, reverse=True)

# loop over the contours

for c in cnts:
	# compute the bounding box of the contour and use the contour to
	# compute the aspect ratio and coverage ratio of the bounding box
	# width to the width of the image
	(x, y, w, h) = cv2.boundingRect(c)
	ar = w / float(h)
	crWidth = w / float(gray.shape[1])

	# check to see if the aspect ratio and coverage width are within
	# acceptable criteria
	if ar > 5 and crWidth > 0.6:
		# pad the bounding box since we applied erosions and now need
		# to re-grow it
		pX = int((x + w) * 0.03)
		pY = int((y + h) * 0.03)
		(x, y) = (x - pX, y - pY)
		(w, h) = (w + (pX * 2), h + (pY * 2))

		# extract the ROI from the image and draw a bounding box
		# surrounding the MRZ
		roi = image[y:y + h, x:x + w]
		print([y,y ,h, x,x , w])
		# mask = np.zeros(image[y:y + h, x:x + w], np.uint8)
		# img=np.array( roi )
		blur = cv2.GaussianBlur(roi, (51,51), 0) 

# Insert ROI back into image
		image[y:y+h, x:x+w] = blur
		# pblur = image[11:11+154,419:419+294]
		# blurno = cv2.GaussianBlur(pblur, (51,51), 0)
		# image[11:11+154,419:419+294]=blurno
		cv2.imwrite(morph_path, image)
		cv2.imshow("ROI11", image)
		cv2.waitKey(0)
		# cv2.imshow("blur",blur)
		# cv2.imshow("image:",image)
		# cv2.rectangle(image, (x,y), (x + w,y + h), (255,255,255), -1)
		# cv2.rectangle(image, (600, 116) , (771, 92), (255,255,255),-1)
		# cv2.rectangle(image, (519, 91), (641, 69), (255,255,255),-1)
		# cv2.rectangle(image, (531, 71) ,(644, 49), (255,255,255),-1)
		#cv2.rectangle(image, (x, y), (x + w, y + h), (100, 255, 190), 3)
		# cv2.drawContours(image, roi,0, (100, 255, 190), 3)
		# cv2.floodFill(image, mask, (0,0), 255)
		break
if not os.path.exists(morph_path):
	
	roi = image[400:400 + 123, 18:18 + 764]
	print("[y,y ,h, x,x , w]")
	# mask = np.zeros(image[y:y + h, x:x + w], np.uint8)
	# img=np.array( roi )
	blur = cv2.GaussianBlur(roi, (51,51), 0) 

# Insert ROI back into image
	image[400:400 + 123, 18:18 + 764]= blur
	# pblur = image[11:11+138,419:419+294]
	# blurno = cv2.GaussianBlur(pblur, (51,51), 0)
	# image[11:11+138,419:419+294]=blurno
	cv2.imwrite(morph_path, image)
	cv2.imshow("ROI", image)
	cv2.waitKey(0)	
# except:
# 	pass
# 	roi = image[400:400 + 113, 18:18 + 764]
# 	print("lmklm")
# 	# mask = np.zeros(image[y:y + h, x:x + w], np.uint8)
# 	# img=np.array( roi )
# 	blur = cv2.GaussianBlur(roi, (51,51), 0) 

# # Insert ROI back into image
# 	image[400:400 + 113, 18:18 + 764]= blur



# pblur = image[11:11+138,419:419+294]
# blurno = cv2.GaussianBlur(pblur, (51,51), 0)
# image[11:11+138,419:419+294]=blurno
# # cv2.rectangle(image, (519, 91), (641, 69), (255,255,255),-1)
# # show the output images
# # cv2.rectangle(image, (600, 116) , (771, 92), (255,255,255),-1)
# # cv2.rectangle(image, (519, 91), (651, 99), (255,255,255),-1)
# # cv2.rectangle(image, (531, 71) ,(654, 99), (255,255,255),-1)
# cv2.imwrite("Image.jpeg", image)
# cv2.imshow("ROI", image)
# cv2.waitKey(0)
#(604, 116) (771, 92)
#(519, 91) (641, 69)
