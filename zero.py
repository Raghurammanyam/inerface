from google.cloud import storage
import os
import re
os.environ["GOOGLE_APPLICATION_CREDENTIALS"]="keelaa.json"


def download_blob(source_blob_name, destination_file_name):

    """Downloads a blob from the bucket."""
    storage_client = storage.Client()
    bucket = storage_client.get_bucket('keelaa-images')
    blob = bucket.blob(source_blob_name)

    blob.download_to_filename(destination_file_name)

    print('Blob {} downloaded to {}.'.format(
        source_blob_name,
        destination_file_name))
    return destination_file_name

url='https://storage.cloud.google.com/keelaa-images/Treck/ganesh8/IMG_20190914_062840.jpg'
source_blob_name=re.sub('https://storage.cloud.google.com/keelaa-images/','',url)

download_blob(source_blob_name,'img.jpg')